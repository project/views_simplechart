# Views Simple Chart

This is simple **Bar Chart**,**Pie Chart**,**Column Chart**,**Timeline Chart** and **Organization Chart** Visualization based on Views. 

The module uses Google Graph API https://developers.google.com/chart for visualizations. No third party libraries are required because Google Graph library is already included.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Maintainers 

- Kostia Bohach ([_shy](https://www.drupal.org/u/_shy))
- Washim Ahmed ([drupwash](https://www.drupal.org/u/drupwash))
- Meenu Gupta ([iammeenugupta96](https://www.drupal.org/u/iammeenugupta96))
- Jacob Embree ([jacob.embree](https://www.drupal.org/u/jacobembree))
